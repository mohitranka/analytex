from django.conf.urls import patterns, url

urlpatterns = patterns('apis.views',
    url(r'^summarize/$', 'summarize'),
)
