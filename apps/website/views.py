# Create your views here.
from django.shortcuts import render_to_response

def index(self):
    return render_to_response('index.html')

def pricing(self):
    return render_to_response('pricing.html')

def docs(self):
    return render_to_response('docs.html')

def contact(self):
    return render_to_response('contact.html')
