from django.conf.urls import patterns, include, url
from settings import STATIC_ROOT

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'apps.website.views.index', name='index'),
    url(r'^pricing/', 'apps.website.views.pricing', name='pricing'),
    url(r'^contact/', 'apps.website.views.contact', name='contact'),
    url(r'^docs/', 'apps.website.views.docs', name='docs'),
    url(
        r'^static/(?P<path>.*)$',
        'django.views.static.serve',
        {'document_root': STATIC_ROOT}
    ),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
