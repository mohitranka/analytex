# Create your views here.
from rest_framework.decorators import api_view
from rest_framework.response import Response
from pyteaser import Summarize


@api_view(['GET', 'POST'])
def summary(data, title='', format=None):
    return Response(Summarize(title, data))
